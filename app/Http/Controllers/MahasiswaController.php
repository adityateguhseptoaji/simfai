<?php

namespace App\Http\Controllers;
use App\Models\Mahasiswa;
use App\Models\Jurusan;
use Illuminate\Http\Request;
use Carbon\Carbon;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mahasiswas = Mahasiswa::all();
        return view('mahasiswa.index',['mahasiswas'=>$mahasiswas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jurusans = Jurusan::all();
        return view('mahasiswa.create',['jurusans'=>$jurusans]);
    }




    public function store(Request $request)
    {
        $request->validate([
            'nim'=> 'required|unique:mahasiswa|max:11',
            'nama'=> 'required',
            'jalur'=> 'required',
            'tempat_lahir'=> 'required',
            'tangal_lahir',
            'jenis_kelamin'=> 'required',
            'tahun_masuk'=> 'required',
            'status'=> 'required',
            'kelurahan'=> 'required',
            'email'=> 'required',
            'alamat'=> 'required',
            'no_hp'=> 'required',
            'no_ktp'=> 'required',
            'nama_ibu'=> 'required',
            'kecamatan'=> 'required',
        ]);
        $input                  = $request->all();
        $mahasiswa              = Mahasiswa::create($input);
        return back()->with('sukses', 'Data Mahasiswa Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Munaqosyah  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function show(Mahasiswa $mahasiswa)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Munaqosyah  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(Mahasiswa $mahasiswa)
    {
        $mahasiswas=Mahasiswa::all();
        $jurusans = Jurusan::all();
        return view('mahasiswa.edit',['mahasiswas'=>$mahasiswas,'jurusans'=>$jurusans]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Munaqosyah  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mahasiswa $mahasiswa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Munaqosyah  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mahasiswa $mahasiswa)
    {
        //
    }
}
