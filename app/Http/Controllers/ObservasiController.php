<?php

namespace App\Http\Controllers;

use App\Models\Observasi;
use Illuminate\Http\Request;

class ObservasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('observasi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('observasi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Observasi  $observasi
     * @return \Illuminate\Http\Response
     */
    public function show(Observasi $observasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Observasi  $observasi
     * @return \Illuminate\Http\Response
     */
    public function edit(Observasi $observasi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Observasi  $observasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Observasi $observasi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Observasi  $observasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Observasi $observasi)
    {
        //
    }
}
