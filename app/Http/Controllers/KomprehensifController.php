<?php

namespace App\Http\Controllers;

use App\Models\Komprehensif;
use Illuminate\Http\Request;

class KomprehensifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('komprehensif.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('komprehensif.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Komprehensif  $komprehensif
     * @return \Illuminate\Http\Response
     */
    public function show(Komprehensif $komprehensif)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Komprehensif  $komprehensif
     * @return \Illuminate\Http\Response
     */
    public function edit(Komprehensif $komprehensif)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Komprehensif  $komprehensif
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Komprehensif $komprehensif)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Komprehensif  $komprehensif
     * @return \Illuminate\Http\Response
     */
    public function destroy(Komprehensif $komprehensif)
    {
        //
    }
}
