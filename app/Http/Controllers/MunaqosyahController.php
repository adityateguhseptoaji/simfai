<?php

namespace App\Http\Controllers;

use App\Models\Munaqosyah;
use Illuminate\Http\Request;

class MunaqosyahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('munaqosyah.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('munaqosyah.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Munaqosyah  $munaqosyah
     * @return \Illuminate\Http\Response
     */
    public function show(Munaqosyah $munaqosyah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Munaqosyah  $munaqosyah
     * @return \Illuminate\Http\Response
     */
    public function edit(Munaqosyah $munaqosyah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Munaqosyah  $munaqosyah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Munaqosyah $munaqosyah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Munaqosyah  $munaqosyah
     * @return \Illuminate\Http\Response
     */
    public function destroy(Munaqosyah $munaqosyah)
    {
        //
    }
}
