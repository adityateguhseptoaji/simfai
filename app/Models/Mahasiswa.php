<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    use HasFactory;
    protected $fillable = [
        'nim',
        'jurusan',
        'nama',
        'jalur',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'tahun_masuk',
        'status',
        'kelurahan',
        'email',
        'alamat',
        'no_hp',
        'no_ktp',
        'nama_ibu',
        'kecamatan',
    ];
    protected $table = 'mahasiswas';
}
