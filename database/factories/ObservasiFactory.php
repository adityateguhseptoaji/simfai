<?php

namespace Database\Factories;

use App\Models\Observasi;
use Illuminate\Database\Eloquent\Factories\Factory;

class ObservasiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Observasi::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
