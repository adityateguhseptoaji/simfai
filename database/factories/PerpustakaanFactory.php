<?php

namespace Database\Factories;

use App\Models\Perpustakaan;
use Illuminate\Database\Eloquent\Factories\Factory;

class PerpustakaanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Perpustakaan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
