<?php

namespace Database\Factories;

use App\Models\Munaqosyah;
use Illuminate\Database\Eloquent\Factories\Factory;

class MunaqosyahFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Munaqosyah::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
