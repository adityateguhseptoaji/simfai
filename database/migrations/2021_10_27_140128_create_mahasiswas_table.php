<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMahasiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswas', function (Blueprint $table) {
            $table->id();
            $table->string('nim')->unique();
            $table->string('jurusan')->nullable();
            $table->string('nama');
            $table->string('jalur');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir')->nullable();
            $table->string('jenis_kelamin');
            $table->integer('tahun_masuk');
            $table->string('status');
            $table->string('kelurahan');
            $table->string('email');
            $table->string('alamat');
            $table->string('no_hp');
            $table->string('no_ktp');
            $table->string('nama_ibu');
            $table->string('kecamatan');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswas');
    }
}
