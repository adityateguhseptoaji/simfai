@extends('layouts.mahasiswa')
@section('title','Membuat Pengajuan Munaqosyah')
@section('content')

<form>
  <div class="row mb-3">
    <label for="inputEmail3" class="col-sm-2 col-form-label">IPK Saat Ini (Contoh: 3,75)</label>
    <div class="col-sm-10">
      <input type="input" class="form-control" id="ipk_munaqosyah">
    </div>
  </div>
  <div class="row mb-3">
    <label for="judul_skripsi_indonesia" class="col-sm-2 col-form-label">Judul Skripsi (Bahasa Indonesia)</label>
    <div class="col-sm-10">
      <textarea  class="form-control" id="judul_skripsi_indonesia"></textarea>
    </div>
  </div>
  <div class="row mb-3">
    <label for="judul_skripsi_inggris" class="col-sm-2 col-form-label">Judul Skripsi (Bahasa Inggris)</label>
    <div class="col-sm-10">
      <textarea  class="form-control" id="judul_skripsi_inggris"></textarea>
    </div>
  </div>
    <div class="row mb-3">
    <label for="inputPassword3" class="col-sm-2 col-form-label">Dosen Penguji 1</label>
    <div class="col-sm-10">
        <select class="form-select" aria-label="Default select example">
            <option selected>Klik untuk memilih</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
    </div>
  </div>
     <div class="row mb-3">
    <label for="inputPassword3" class="col-sm-2 col-form-label">Dosen Penguji 2</label>
    <div class="col-sm-10">
        <select class="form-select" aria-label="Default select example">
            <option selected>Klik untuk memilih</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Buat Surat</button>
</form>

@endsection
