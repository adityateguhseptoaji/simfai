<html ml-update="aware">

@include('layouts.admin.header')

    <body>

        @include('layouts.perpustakaan.navbar')

        <div class="container">
            <div class="columns">
                @include('layouts.admin.sidebar')
                <div class="column is-9">

                    @include('layouts.admin.breadcumb')

                    {{-- @include('admin.layouts.welcome') --}}

                    @yield('content')

                </div>
            </div>
        </div>
        @include('layouts.footer')
    </body>

    </html>
