<html ml-update="aware">

@include('layouts.admin.header')

    <body>
        @include('layouts.perpustakaan.navbar')

        @yield('content')

        @include('layouts.footer')
    </body>

</html>
