@extends('perpustakaan.kunjungan.app')
@section('content')

<div class="block">
    <section class="hero is-link">
        <div class="hero-body">
            <p class="title">
                Kunjungan Readingroom
            </p>
            <p class="subtitle">
                Selamat Datang :)
            </p>
        </div>
    </section>
</div>

<div class="columns has-text-centered">
    <div class="column">
        <div class="field">
            <article class="message is-primary">
                <div class="message-header">
                    <p>Kedatangan</p>
                </div>
                <div class="message-body">
                    <p class="control has-icons-left has-icons-right">
                        <div class="block">
                            <label class="label">Masukan NIM Anda</label>
                        </div>
                        <div class="block">
                            <input class="input" type="waktu_datang" placeholder="Nomor Induk Mahasiswa">
                        </div>
                        <div class="block">
                            <button class="button is-primary">Masukan</button>
                        </div>
                    </p>
                </div>
            </article>
        </div>
    </div>

    <div class="column">
        <div class="field">
            <article class="message is-warning">

                <div class="message-header">
                    <p>Kepulangan</p>
                </div>
                <div class="message-body">
                    <p class="control has-icons-left">
                        <div class="block">
                            <label class="label">Masukan NIM Anda</label>
                        </div>
                        <div class="block">
                            <input class="input" type="waktu_pulang" placeholder="Nomor Induk Mahasiswa">
                        </div>
                        <div class="block">
                            <button class="button is-warning">Masukan</button>
                        </div>
                    </p>
                </div>
            </article>
        </div>
    </div>
</div>

@endsection
