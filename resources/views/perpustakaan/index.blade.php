@extends('perpustakaan.app')
@section('content')

<div class="columns has-text-centered">
    <div class="column">
        <div class="card has-background-danger-dark">
            <div class="card-content">
                <div class="content">
                    <h1 class="has-text-white">Data Perpus</h1>
                </div>
            </div>
        </div>
        <div class="card has-background-link">
            <div class="card-content">
                <div class="content">
                    <h1 class="has-text-white"><a class="has-text-white" href="{{route('kunjungan.index')}}">Daftar Kunjungan</a></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="card has-background-success-dark">
            <div class="card-content">
                <div class="content">
                <h1 class="has-text-white">Peminjaman</h1>
                </div>
            </div>
        </div>
        <div class="card has-background-warning-dark">
            <div class="card-content">
                <div class="content">
                <h1 class="has-text-white">Pengaturan</h1>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
