@extends('layouts.mahasiswa')
@section('title','Membuat Pengajuan Penelitian')
@section('content')

<div class="Scroll">
    <form>
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Judul Penelitian</label>
            <div class="col-sm-10">
                <textarea type="input" class="form-control" id="judul_penelitian"></textarea>
            </div>
        </div>
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">IPK Saat ini</label>
            <div class="col-sm-3">
                <input type="input" class="form-control" id="ipk_komprehensif">
            </div>
        </div>
        <hr>
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">
                <h4>Tujuan Surat</h3>
            </label>
        </div>
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Kepada</label>
            <div class="col-sm-2">
                <select class="form-select" aria-label="Default select example">
                    <option selected>Silahkan pilih</option>
                    <option value="Kepala">Kepala</option>
                    <option value="Ketua">Ketua</option>
                </select>
            </div>
        </div>
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Lembaga</label>
            <div class="col-sm-10">
                <input type="input" class="form-control" id="nama_lembaga">
            </div>
        </div>
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Alamat Lembaga</label>
            <div class="col-sm-10">
                <textarea type="input" class="form-control" id="alamat_lembaga"></textarea>
            </div>
        </div>
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Kota</label>
            <div class="col-sm-3">
                <input type="input" class="form-control" id="kota_lembaga">
            </div>
        </div>
        <hr>
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Tembusan 1</label>
            <div class="col-sm-3">
                <input type="input" class="form-control" id="tembusan_1">
            </div>
        </div>
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Tembusan 2</label>
            <div class="col-sm-3">
                <input type="input" class="form-control" id="tembusan_2">
            </div>
        </div>
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Tembusan 3</label>
            <div class="col-sm-3">
                <input type="input" class="form-control" id="tembusan_3">
            </div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">Buat Surat</button>
        </div>
    </div>
</form>
</div>
@endsection
