@extends('layouts.mahasiswa')\
@section('title','Membuat Pengajuan Ujian Komprehensif')
@section('content')

<form>
  <div class="row mb-3">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Jumlah SKS</label>
    <div class="col-sm-10">
      <input type="input" class="form-control" id="sks_komprehensif" placeholder="Isikan dengan Jumlah SKS yang diambil saaat ini">
    </div>
  </div>
    <div class="row mb-3">
    <label for="inputEmail3" class="col-sm-2 col-form-label">IPK Saat ini</label>
    <div class="col-sm-10">
      <input type="input" class="form-control" id="ipk_komprehensif" placeholder="Isikan dengan titik, misal 3.75">
    </div>
  </div>
    <button type="submit" class="btn btn-primary">Buat Surat</button>
</form>

@endsection
