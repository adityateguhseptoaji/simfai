@extends('layouts.admin')
@section('title','Detail Data Mahasiswa')
@section('content')


<div class="Scroll">
    <form method="POST" action="{{url ('mahasiswa')}}">
        @csrf
        <br>
        <div class="container">
            @foreach ($mahasiswas as $mahasiswa)
            <div class="row">

                <div class="col-sm">
                    <h4 class="text-center">Data Perkuliahan</h4>
                    <hr>
                    <br>

                    <div class="form-group row">
                        <label for="nim" class="col-sm-3 col-form-label">NIM</label>
                        <div class="col-sm">
                            <input name="nim" type="text" class="form-control" id="nim" value="{{$mahasiswa->nim}}">
                        </div>
                    </div>

                    <div class="form-group row">
                    <label for="no_ktp" class="col-sm-3 col-form-label">No KTP</label>
                    <div class="col-sm">
                        <input name="no_ktp" type="text"  class="form-control" id="no_ktp" value="{{$mahasiswa->no_ktp}}">
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="nama" class="col-sm-3 col-form-label">Nama Lengkap</label>
                    <div class="col-sm">
                        <input name="nama" type="text"  class="form-control" id="nama" value="{{$mahasiswa->nama}}">
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="jurusan" class="col-sm-3 col-form-label">Program Studi</label>
                    <div class="col-sm">
                        <select name="jurusan" class="form-control" id="inputState">
                            <option selected>{{$mahasiswa->jurusan}}</option>
                            @foreach ($jurusans as $jurusan)
                            <option value="{{$jurusan->id}}">{{$jurusan->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    </div>


                    <div class="form-group row">
                    <label for="jalur" class="col-sm-3 col-form-label">Jalur Masuk</label>
                    <div class="col-sm">
                        <select name="jalur" id="inputState" class="form-control">
                            <option selected>{{$mahasiswa->jalur}}</option>
                            <option>Mant</option>
                        </select>
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="tahun_masuk" class="col-sm-3 col-form-label">Tahun Masuk</label>
                    <div class="col-sm">
                        <input name="tahun_masuk" type="text"  class="form-control" id="tahun_masuk" value="{{$mahasiswa->tahun_masuk}}">
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="no_hp" class="col-sm-3 col-form-label">No Telepon</label>
                    <div class="col-sm">
                        <input name="no_hp" type="text"  class="form-control" id="no_hp" value="{{$mahasiswa->no_hp}}">
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="email" class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm">
                        <input name="email" type="text"  class="form-control" id="email" value="{{$mahasiswa->email}}">
                    </div>
                    </div>

                </div>

                    <div class="vr"></div>
                <div class="col-sm">
                    <h4 class="text-center">Tempat Tinggal</h4>
                    <hr>
                    <br>

                    <div class="form-group row">
                    <label for="nama_ibu" class="col-sm-3 col-form-label">Nama Ibu</label>
                    <div class="col-sm">
                        <input name="nama_ibu" type="text"  class="form-control" id="nama_ibu" value="{{$mahasiswa->nama_ibu}}">
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="tempat_lahir" class="col-sm-3 col-form-label">Tempat Lahir</label>
                    <div class="col-sm">
                        <input name="tempat_lahir" type="text"  class="form-control" id="tempat_lahir" value="{{$mahasiswa->tempat_lahir}}">
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="tanggal_lahir" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                    <div class="col-sm">
                        <input type="date" name="tanggal_lahir"   class="form-control" id="tanggal_lahir" value="{{$mahasiswa->tanggal_lahir}}">
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="jenis_kelamin" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm">
                        <select name="jenis_kelamin" id="inputState" class="form-control">
                            <option selected>{{$mahasiswa->jenis_kelamin}}</option>
                            <option>Laki-laki</option>
                            <option>Perempuan</option>
                        </select>
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="status" class="col-sm-3 col-form-label">Status</label>
                    <div class="col-sm">
                        <select name="status" id="inputState" class="form-control">
                            <option selected>value="{{$mahasiswa->status}}"</option>
                            <option>status</option>
                        </select>
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                    <div class="col-sm">
                        <textarea name="alamat" class="form-control" id="alamat">{{$mahasiswa->alamat}}
                        </textarea>
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="kelurahan" class="col-sm-3 col-form-label">Kelurahan</label>
                    <div class="col-sm">
                        <input name="kelurahan" type="text"  class="form-control" id="kelurahan" value="{{$mahasiswa->kelurahan}}">
                    </div>
                    </div>

                    <div class="form-group row">
                    <label for="kecamatan" class="col-sm-3 col-form-label">Kecamatan</label>
                    <div class="col-sm">
                        <input name="kecamatan" type="text"  class="form-control" id="kecamatan" value="{{$mahasiswa->kecamatan}}">
                    </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-warning mb-4">Update Data</button>
        </div>
    </form>
</div>
@endsection
