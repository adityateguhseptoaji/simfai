@extends('layouts.admin')
@section('title','Daftar Mahasiswa')
@section('content')
<div>
    <button type="button" class="btn btn-success" onclick=location.href='{{ route('mahasiswa.create')}}'>Tambah Mahasiswa Baru</button>
</div>
<br>
<div class="wrapper Scroll">
    <table class="table table-bordered table-striped table-hover">
    <thead>
        <tr class="text-center">
        <th>NIM</th>
        <th>KTP</th>
        <th>Nama Lengkap</th>
        <th>Program Studi</th>
        <th>Jalur Masuk</th>
        <th>Tahun Masuk</th>
        <th>Telepon</th>
        <th>Email</th>
        <th colspan="2">Operasi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($mahasiswas as $mahasiswa)
        <tr>
        <td style="width: 10%" >{{ $mahasiswa->nim}}</td>
        <td style="width: 10%" >{{ $mahasiswa->no_ktp}}</td>
        <td style="width: 20%" >{{ $mahasiswa->nama }}</td>
        <td style="width: 10%" >{{ $mahasiswa->jurusan }}</td>
        <td style="width: 10%" >{{ $mahasiswa->jalur }}</td>
        <td style="width: 10%" >{{ $mahasiswa->tahun_masuk }}</td>
        <td style="width: 10%" >{{ $mahasiswa->no_hp }}</td>
        <td style="width: 10%" >{{ $mahasiswa->email }}</td>
        <td style="width: 5%">
            <button style="width: 100%" class="btn btn-warning" onclick=location.href='{{ route('mahasiswa.edit',$mahasiswa)}}'>
            <i class="ti ti-menu-alt"></i></button>
        </td>
        <td style="width: 5%">
            <button style="width: 100%" class="btn btn-danger"><i class="ti ti-trash"></i></button>
        </td>
        @endforeach
    </tbody>
    </table>
</div>
@endsection
