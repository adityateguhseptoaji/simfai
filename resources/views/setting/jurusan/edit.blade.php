@extends('layouts.admin')
@section('title','Merubah Data Jurusan')
@section('content')

<form method="POST" action="{{ route('jurusan.index') }}">
    @csrf
    @method('PUT')
    <div class="row mb-3">
        <label for="nama" class="col-sm-2 col-form-label">Nama Jurusan</label>
        <div class="col-sm-10">
            <input name="nama" value="{{$post->nama}}" type="input" class="form-control" id="nama">
        </div>
    </div>
    <div class="row mb-3">
        <label for="strata" class="col-sm-2 col-form-label">Strata</label>
        <div class="col-sm-10">
            <select name="strata" class="form-select" aria-label="Default">
                <option selected value="S1">S1</option>
                <option value="S2">S2</option>
                <option value="S3">S3</option>
            </select>
        </div>
    </div>
    <div class="row mb-3">
        <label for="strata" class="col-sm-2 col-form-label">Ketua Prodi</label>
        <div class="col-sm-10">
            <select name="ketua" class="form-select" aria-label="Default">
                <option selected value="Ahmad">Ahmad</option>
                <option value="Beta">Beta</option>
                <option value="Gamma">Gamma</option>
            </select>
        </div>
    </div>
    <div class="row mb-3">
        <label for="nomor_akreidtasi" class="col-sm-2 col-form-label">Nomor Pengesahan BNPT</label>
        <div class="col-sm-10">
            <input name="no_bnpt" value="{{$post->no_bnpt}}" type="text" class="form-control" id="nomor_akreditasi">
        </div>
    </div>
    <div class="row mb-3">
        <label for="akreditasi" class="col-sm-2 col-form-label">Akreditasi</label>
        <div class="col-sm-10">
            <select name="akreditasi" class="form-select" aria-label="Default">
                <option selected value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
            </select>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Buat Program Studi</button>
</form>

@endsection

