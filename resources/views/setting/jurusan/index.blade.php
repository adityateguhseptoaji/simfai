@extends('layouts.admin')
@section('title','Pengaturan Program Studi')
@section('content')

<div class="">
    <button type="button" class="btn btn-success" onclick=location.href='{{route('jurusan.create')}}' value="Tambah Jurusan Baru">
        Tambah Jurusan Baru
    </button>
</div>
<br>
<div class="wrapper">
    <table style="width:100%">
    <thead>
        <tr>
        <th>Nama Jurusan</th>
        <th>Ketua Program Studi</th>
        <th>Strata</th>
        <th>Nomor BNPT</th>
        <th>Akreditasi</th>
        <th></th>
        <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($jurusans as $jurusan)
        <tr>
            <td style="width: 500px" >{{$jurusan->nama}}</td>
            <td style="width: 500px" >{{$jurusan->ketua}}</td>
            <td style="width: 500px" >{{$jurusan->strata}}</td>
            <td style="width: 500px" >{{$jurusan->no_bnpt}}</td>
            <td style="width: 500px" >{{$jurusan->akreditasi}}</td>
            <td style="width: 100px"><button class="btn btn-success">Edit</button></td>
            <td style="width: 100px"><button class="btn btn-danger">Hapus</button></td>
            </tr>
        @endforeach
    </tbody>
    </table>
</div>
@endsection
