@extends('layouts.admin')
@section('title','Pengaturan Data Dosen')
@section('content')
<div class="">
    <button type="button" class="btn btn-success" href="{{route('dosen.create')}}" value="Tambah Dosen Baru">
        Tambah Dosen Baru
    </button>
</div>
<br>
<div class="wrapper">
    <table style="width:100%">
    <thead>
        <tr>
        <th>NIM</th>
        <th>Nama</th>
        <th>Jurusan</th>
        <th></th>
        <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
        <td style="width: 200px" >nim</td>
        <td style="width: 500px" >nama</td>
        <td style="width: 500px" >jurusan</td>
        <td style="width: 100px"><button class="btn btn-success">Edit</button></td>
        <td style="width: 100px"><button class="btn btn-danger">Hapus</button></td>
        </tr>
    </tbody>
    </table>
</div>
@endsection
