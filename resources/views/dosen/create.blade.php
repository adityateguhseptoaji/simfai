@extends('layouts.mahasiswa')
@section('title','Data Mahasiswa Baru')
@section('content')

<div class="Scroll">
    <form method="POST" action="{{url ('mahasiswa')}}">
        @csrf
        <br>
        <h4>Perkuliahan</h4>
        <hr>
        <div class="form-group row">
            <label for="nim" class="col-sm-2 col-form-label">NIM</label>
            <div class="col-sm-10">
                <input name="nim" type="text" class="form-control" id="nim">
            </div>
        </div>
        <div class="form-group row">
            <label for="jurusan" class="col-sm-2 col-form-label">Program Studi</label>
            <div class="col-sm-10">
                <select name="jurusan" id="inputState" class="form-control">
                    <option selected>Choose</option>
                    <option>jurusan</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="nama" class="col-sm-2 col-form-label">Nama Lengkap</label>
            <div class="col-sm-10">
                <input name="nama" type="text"  class="form-control" id="nama">
            </div>
        </div>
        <div class="form-group row">
            <label for="jalur" class="col-sm-2 col-form-label">Jalur Masuk</label>
            <div class="col-sm-10">
                <select name="jalur" id="inputState" class="form-control">
                    <option selected>Choose</option>
                    <option>jalur</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="tahun_masuk" class="col-sm-2 col-form-label">Tahun Masuk</label>
            <div class="col-sm-10">
                <input name="tahun_masuk" type="text"  class="form-control" id="tahun_masuk">
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input name="email" type="text"  class="form-control" id="email">
            </div>
        </div>
        <br>
        <h4>Data Diri</h4>
        <hr>
        <div class="form-group row">
            <label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
            <div class="col-sm-10">
                <input name="tempat_lahir" type="text"  class="form-control" id="tempat_lahir">
            </div>
        </div>
        <div class="form-group row">
            <label for="tanggal_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
            <div class="col-sm-10">
                <input name="tanggal_lahir" type="date"  class="form-control" id="tanggal_lahir">
            </div>
        </div>
        <div class="form-group row">
            <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
            <div class="col-sm-10">
                <select name="jenis_kelamin" id="inputState" class="form-control">
                    <option selected>Choose</option>
                    <option>Laki-laki</option>
                    <option>Perempuan</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="status" class="col-sm-2 col-form-label">Status</label>
            <div class="col-sm-10">
                <select name="status" id="inputState" class="form-control">
                    <option selected>Choose</option>
                    <option>jalur</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="nama_ibu" class="col-sm-2 col-form-label">Nama Ibu</label>
            <div class="col-sm-10">
                <input name="nama_ibu" type="text"  class="form-control" id="nama_ibu">
            </div>
        </div>
        <div class="form-group row">
            <label for="kelurahan" class="col-sm-2 col-form-label">Kelurahan</label>
            <div class="col-sm-10">
                <input name="kelurahan" type="text"  class="form-control" id="kelurahan">
            </div>
        </div>
        <div class="form-group row">
            <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
            <div class="col-sm-10">
                <textarea name="alamat" class="form-control" id="alamat">
                </textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="no_hp" class="col-sm-2 col-form-label">No Telepon</label>
            <div class="col-sm-10">
                <input name="no_hp" type="text"  class="form-control" id="no_hp">
            </div>
        </div>
        <div class="form-group row">
            <label for="no_ktp" class="col-sm-2 col-form-label">No KTP</label>
            <div class="col-sm-10">
                <input name="no_ktp" type="text"  class="form-control" id="no_ktp">
            </div>
        </div>
        <div class="form-group row">
            <label for="kecamatan" class="col-sm-2 col-form-label">Kecamatan</label>
            <div class="col-sm-10">
                <input name="kecamatan" type="text"  class="form-control" id="kecamatan">
            </div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary mb-3">Tambah Data</button>
        </div>
    </form>
</div>
<script>
    $(document).ready(function () {
      $('.tanggal_lahir').datetimepicker({
        format: 'MM/DD/YYYY',
        locale: 'en'
      });
    </script>
@endsection
