<div id="navbarMenu" class="navbar-menu">
    <div class="navbar-end">
        <div class="tabs is-right">
            <ul>
                <li class="is-active"><a>Beranda</a></li>
                <li><a href="">Kemahasiswaan</a></li>
                <li><a href="{{ route ('perpustakaan.index')}}">Perpustakaan</a></li>
                <li><a href="{{ route ('admin.index')}}">Admin</a></li>
                <li><a href="">Tentang Aplikasi</a></li>
            </ul>
        </div>
    </div>
</div>
