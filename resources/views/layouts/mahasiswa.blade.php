<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <title>@yield('title')</title>

    <link href="{{asset('/css/app.css')}}" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicon.png')}}">
</head>

<body class="theme-blue">

    {{-- validasi --}}
    @if (session('success'))
    <div class="alert-success">
    <p>{{ session('success') }}</p>
    </div>
    @endif

    @if ($errors->any())
    <div class="alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
    </div>
    @endif
{{-- end - validasi --}}

    <div class="grid-wrapper sidebar-bg bg1">

        <!-- BOF HEADER -->
        @include('layouts.mahasiswa_header')
        <!-- EOF HEADER -->

        <!-- BOF ASIDE-LEFT -->
        @include('layouts.mahasiswa_sidebar')
        <!-- EOF ASIDE-LEFT -->

        <!-- BOF MAIN -->
        <div class="main">

        <!-- BOF Breadcrumb -->
        @include('layouts.mahasiswa_breadcumb')
        <!-- EOF Breadcrumb -->

        <!-- BOF MAIN-BODY -->
        @yield('content')
        <!-- EOF MAIN-BODY -->
        </div>
        <!-- EOF MAIN -->

        <!-- BOF FOOTER -->
        @include('layouts.mahasiswa_footer')
        <!-- EOF FOOTER -->

        <!-- BOF ASIDE-RIGHT -->
        @include('layouts.mahasiswa_setting')
        <!-- EOF ASIDE-RIGHT -->

        <div id="overlay"></div>


    </div> <!-- END WRAPPER -->

    <script src="{{asset('/js/app.js')}}"></script>
</body>

</html>
