<div id="sidebar" class="sidebar">
    <div class="sidebar-content">
        <!-- sidebar-menu  -->
        <div class="sidebar-menu">
            <ul>
                <li class="header-menu">
                    <span>Menu Mahasiswa</span>
                </li>
                <li class="active">
                    <a href="{{ route('mahasiswa.index')}}">
                        <i class="ti-dashboard"></i>
                        <span class="menu-text">Beranda</span>
                    </a>
                </li>
                <li class="maincat">
                    <a href="#">
                        <i class="ti-agenda"></i>
                        <span class="menu-text">Administrasi</span>
                    </a>
                    <div class="subcat">
                        <ul>
                            <li>
                                <a href="{{ route('munaqosyah.create')}}">Pendaftaran Munaqosah</a>
                            </li>
                            <li class="tier1">
                                <a href="javascript:;">Surat Menyurat</a>
                                <div class="subcat">
                                    <ul>
                                        <li><a href="{{ route('penelitian.create')}}">Surat Izin Penelitian</a></li>
                                        <li><a href="{{ route ('observasi.create')}}">Surat Izin Observasi</a></li>
                                        <li><a href="javascript:;">Surat Ujian Susulan</a></li>
                                        <li><a href="javascript:;">Surat Keterangan Lulus</a></li>
                                        <li><a href="javascript:;">Surat Bimbingan Skripsi</a></li>
                                        <li><a href="javascript:;">Surat Izin Praktek</a></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a href="{{ route('komprehensif.create')}}">Pendaftaran Komprehensif</a>
                            </li>
                        </ul>
                    </div>
                </li>
                {{-- <li>
                    <a href="documenation.html">
                        <i class="ti-agenda"></i>
                        <span class="menu-text">Documentation</span>
                        <span class="badge badge-pill badge-primary">Beta</span>
                    </a>
                </li>
                <li>
                    <a href="https://github.com/siQuang/siqtheme" target="_blank">
                        <i class="ti-github"></i>
                        <span class="menu-text">siQtheme Github</span>
                    </a>
                </li> --}}
            </ul>
        </div>
        <!-- sidebar-menu  -->
    </div>
</div>
