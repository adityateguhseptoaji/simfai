        <!-- START NAV -->
        <nav class="navbar is-transparent">
            <div class="container">
                <div class="navbar-brand">
                    <a class="navbar-item brand-text" href="{{route ('home')}}">
                        Sistem Informasi FAI
                    </a>
                    <div class="navbar-burger burger" data-target="navMenu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div id="navMenu" class="navbar-menu">
                    <div class="navbar-start">
                        <a class="navbar-item" href="{{ route('perpustakaan.index')}}">
                            Perpustakaan
                        </a>
                    </div>
                </div>
            </div>
        </nav>
        <!-- END NAV -->
