<div class="column is-3 ">
    <aside class="menu is-hidden-mobile">
        <p class="menu-label">
            Umum
        </p>
        <ul class="menu-list">
            <li><a class="is-active">Dashboard</a></li>
        </ul>
        <p class="menu-label">
            Administrasi
        </p>
        <ul class="menu-list">
            <li>
                <a>Bank Data</a>
                <ul>
                    <li><a href="{{route('mahasiswa.index')}}">Mahasiswa</a></li>
                    <li><a href="#">Dosen</a></li>
                    <li><a href="{{route('jurusan.index')}}">Program Studi</a></li>
                    <li><a href="#">Mata Kuliah</a></li>
                </ul>
            </li>

            <li>
                <a>Surat Menyurat</a>
                <ul>
                    <li><a href="{{ route('munaqosyah.index')}}">Pendaftaran Munaqosah</a></li>
                    <li><a href="{{ route('penelitian.index')}}">Surat Izin Penelitian</a></li>
                    <li><a href="{{ route('observasi.index')}}">Surat Izin Observasi</a></li>
                    <li><a href="javascript:;">Surat Ujian Susulan</a></li>
                    <li><a href="javascript:;">Surat Keterangan Lulus</a></li>
                    <li><a href="javascript:;">Surat Bimbingan Skripsi</a></li>
                    <li><a href="javascript:;">Surat Izin Praktek</a></li>
                    <li><a href="{{ route('komprehensif.index')}}">Pendaftaran Komprehensif</a></li>
                </ul>
            </li>
        </ul>
        <p class="menu-label">
            Perpustakaan
        </p>
        <ul class="menu-list">
            <li><a href="{{ route('kunjungan.create')}}">Kunjungan (Front)</a></li>
            <li><a href="{{ route('kunjungan.index')}}">History Kunjungan</a></li>
        </ul>
    </aside>
</div>
