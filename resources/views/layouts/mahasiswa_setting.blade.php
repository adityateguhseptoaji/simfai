<div id="sidebar-right">
    <div class="sidebar-right-container">

        <!-- BOF TABS -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a href="#tab-1" data-toggle="tab" class="nav-link active">Profil</a>
            </li>
            <li>
                <a href="#tab-2" data-toggle="tab" class="nav-link">Settings</a>
            </li>
            <li>
                <a href="#tab-3" data-toggle="tab" class="nav-link">Help</a>
            </li>
        </ul>
        <!-- EOF TABS -->

        <!-- BOF TAB PANES -->
        <div class="tab-content">
            <!-- BOF TAB-PANE #1 -->
            <div id="tab-1" class="tab-pane show active">
                <div class="pane-header">
                    <h3><i class="ti-user"></i> Mahasiswa</h3>
                    <i class="fa fa-circle text-success"></i>
                    <span class="profile-user">
                        (Nama Mahasiswa)
                    </span>
                    <span class="float-right"><a href="#" class="text-carolina">Log-Out</a></span>
                </div>
                <div class="pane-body">
                    <div class="card bg-transparent mb-3">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <h5 class="mb-3">My Profile</h5>
                                <form class="form-update-profile">
                                    <div class="form-group row">
                                        <label class="col-form-label col-md-4">Firstname:</label>
                                        <div class="col">
                                            <input type="text" name="first_name" class="form-control-plaintext"
                                            value="siQuang">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-md-4">Lastname:</label>
                                        <div class="col">
                                            <input type="text" name="last_name" class="form-control-plaintext"
                                            value="Humbleman">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-md-4">Email:</label>
                                        <div class="col">
                                            <input type="text" name="email" class="form-control-plaintext"
                                            value="siquang@example.com">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-md-4">Username:</label>
                                        <div class="col">
                                            <input type="text" name="username" class="form-control-plaintext"
                                            value="siquang">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-md-4">Password:</label>
                                        <div class="col">
                                            <input type="password" name="password"
                                            class="form-control-plaintext" value="123456789">
                                        </div>
                                    </div>
                                    <div class="col offset-md-4 pl-2">
                                        <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- EOF TAB-PANE #1 -->

            <!-- BOF TAB-PANE #2 -->
            <div id="tab-2" class="tab-pane">
                <div class="pane-header">
                    <h3><i class="ti-settings"></i> Settings</h3>
                    <div class="card bg-transparent mb-3">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <h5 class="mb-3">Pilihan Tema (Beta)</h5>
                                <div class="btn-group mb-2">
                                    <button type="button" class="btn switch-theme btn-light"
                                    id="theme-default">Light</button>
                                    <button type="button" class="btn switch-theme btn-dark"
                                    id="theme-dark">Dark</button>
                                    <button type="button" class="btn switch-theme btn-primary"
                                    id="theme-blue">Blue</button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="pane-body">

                </div>
            </div>
            <!-- EOF TAB-PANE #2 -->

            <!-- BOF TAB-PANE #3 -->
            <div id="tab-3" class="tab-pane">
                <div class="pane-header">
                    <h3><i class="ti-agenda"></i> Help</h3>
                    Frequently Asked Questions
                    <span class="float-right"><a href="#" class="text-carolina">Log-Out</a></span>
                </div>
                <div class="pane-body">
                    <div class="accordion" id="faq-example">
                        <div class="card">
                            <div class="card-header">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#faq1">
                                    Frequently Asked Question #1
                                </button>
                            </h2>
                        </div>
                        <div id="faq1" class="collapse show" data-parent="#faq-example">
                            <div class="card-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
                                dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                                tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                                assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                                wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                                vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
                                synth nesciunt you probably haven't heard of them accusamus labore
                                sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                data-target="#faq2">
                                Frequently Asked Question #2
                            </button>
                        </h2>
                    </div>
                    <div id="faq2" class="collapse" data-parent="#faq-example">
                        <div class="card-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                            richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
                            dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                            tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                            assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                            wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                            vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
                            synth nesciunt you probably haven't heard of them accusamus labore
                            sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#faq3">
                            Frequently Asked Question #3
                        </button>
                    </h2>
                </div>
                <div id="faq3" class="collapse" data-parent="#faq-example">
                    <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
                        dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                        tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                        assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                        wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                        vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
                        synth nesciunt you probably haven't heard of them accusamus labore
                        sustainable VHS.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                        data-target="#faq4">
                        Frequently Asked Question #4
                    </button>
                </h2>
            </div>
            <div id="faq4" class="collapse" data-parent="#faq-example">
                <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
                    dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                    tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                    assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                    wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                    vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
                    synth nesciunt you probably haven't heard of them accusamus labore
                    sustainable VHS.
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h2 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                    data-target="#faq5">
                    Frequently Asked Question #5
                </button>
            </h2>
        </div>
        <div id="faq5" class="collapse" data-parent="#faq-example">
            <div class="card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
                dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
                synth nesciunt you probably haven't heard of them accusamus labore
                sustainable VHS.
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                data-target="#faq6">
                Frequently Asked Question #6
            </button>
        </h2>
    </div>
    <div id="faq6" class="collapse" data-parent="#faq-example">
        <div class="card-body">
            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
            richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
            dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
            tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
            assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
            wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
            vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
            synth nesciunt you probably haven't heard of them accusamus labore
            sustainable VHS.
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h2 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
            data-target="#faq7">
            Frequently Asked Question #7
        </button>
    </h2>
</div>
<div id="faq7" class="collapse" data-parent="#faq-example">
    <div class="card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
        dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
        tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
        assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
        wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
        vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
        synth nesciunt you probably haven't heard of them accusamus labore
        sustainable VHS.
    </div>
</div>
</div>
</div>
</div>
</div>
<!-- EOF TAB-PANE #3 -->
</div>
<!-- EOF TAB PANES -->

</div>
</div>
