<nav aria-label="breadcrumb">
    <h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url()->previous() }}"> Beranda </a></li>
            <li class="breadcrumb-item active" aria-current="page"> @yield('title') </li>
        </ol>
    </h4>
</nav>
