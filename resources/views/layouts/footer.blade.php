<div class="hero-foot">
    <div class="container">
        <footer class="footer">
            <div class="content has-text-centered">
                <p>
                    <strong>SIMFAI</strong> by <a href="https://fai.unissula.ac.id/">Fakultas Agama Islam</a>.
                    <br>
                    The website is associated with <a href="https://unissula.ac.id/">Universitas Islam Sultan Agung</a>.
                </p>
            </div>
        </footer>
    </div>
</div>
