<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\MunaqosyahController;
use App\Http\Controllers\KomprehensifController;
use App\Http\Controllers\PenelitianController;
use App\Http\Controllers\ObservasiController;
use App\Http\Controllers\DosenController;
use App\Http\Controllers\JurusanController;
use App\Http\Controllers\PerpustakaanController;
use App\Http\Controllers\KunjunganController;

Auth::routes();

Route::get('/', [HomeController::class, 'index'])
->name('home');

Route::resources([
    'admin' => AdminController::class
]);

Route::resources([
    'mahasiswa' => MahasiswaController::class
]);

Route::resources([
    'munaqosyah' => MunaqosyahController::class
]);

Route::resources([
    'komprehensif' => KomprehensifController::class
]);

Route::resources([
    'penelitian' => PenelitianController::class
]);
Route::resources([
    'observasi' => ObservasiController::class
]);
Route::resources([
    'dosen' => DosenController::class
]);
Route::resources([
    'jurusan' => JurusanController::class
]);

Route::resources([
    'perpustakaan' => PerpustakaanController::class
]);
Route::resources([
    'kunjungan' => KunjunganController::class
]);

Route::get('/users/{user}/edit', [UserController::class,'edit'])
->middleware('can:update,user');

Route::patch('/users/{user}', [UserController::class,'update'])
->middleware('can:update,user');

Route::delete('/users/{user}', [UserController::class,'destroy'])
->middleware('can:delete,user');
